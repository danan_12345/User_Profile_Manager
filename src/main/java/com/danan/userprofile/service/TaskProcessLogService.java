package com.danan.userprofile.service;

import com.danan.userprofile.bean.TaskProcessLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhangchen
 * @since 2021-04-27
 */
public interface TaskProcessLogService extends IService<TaskProcessLog> {

}
