package com.danan.userprofile.service;

import com.danan.userprofile.bean.FileInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhangchen
 * @since 2021-04-14
 */
public interface FileInfoService extends IService<FileInfo> {

}
