package com.danan.userprofile.service;

import com.danan.userprofile.bean.TaskTagRule;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhangchen
 * @since 2021-04-15
 */
public interface TaskTagRuleService extends IService<TaskTagRule> {

}
