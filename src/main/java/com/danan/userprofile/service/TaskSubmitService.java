package com.danan.userprofile.service;

import com.danan.userprofile.bean.TaskProcess;

public interface TaskSubmitService {


    public void submitTask(TaskProcess taskProcess, boolean isRetry);
}
