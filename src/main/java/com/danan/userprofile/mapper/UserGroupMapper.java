package com.danan.userprofile.mapper;

import com.danan.userprofile.bean.UserGroup;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhangchen
 * @since 2021-05-04
 */


@Mapper
@DS("mysql")
public interface UserGroupMapper extends BaseMapper<UserGroup> {

    @DS("ck")
    void insertResultToCK(@Param("userGroupId") String userGroupId, @Param("querySQL") String querySQL);

    @DS("ck")
    @Update("alter table user_group delete where user_group_id = #{userGroupId}")
    void deleteResultFromCK(@Param("userGroupId") String userGroupId);

    String selectValueTypeByTagCode(@Param("tagCode") String tagCode);

    @DS("ck")
    @Select("select arrayJoin(bitmapToArray(users)) from user_group where user_group_id = #{userGroupId}")
    List<String> selectUPResultById(@Param("userGroupId") String userGroupId);

    @DS("ck")
    @Select("select arrayJoin(bitmapToArray(users)) from (${querySQL}) tmp")
    List<String> selectUPResult(@Param("querySQL") String querySQL);
}
