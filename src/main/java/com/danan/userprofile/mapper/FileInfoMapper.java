package com.danan.userprofile.mapper;

import com.danan.userprofile.bean.FileInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhangchen
 * @since 2021-04-14
 */
@Mapper
public interface FileInfoMapper extends BaseMapper<FileInfo> {

}
