package com.danan.userprofile.mapper;

import com.danan.userprofile.bean.TaskInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhangchen
 * @since 2021-04-15
 */
public interface TaskInfoMapper extends BaseMapper<TaskInfo> {



}
